package com.devcamp.task6070jparelationshipordercustomer.controllers;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.task6070jparelationshipordercustomer.model.CUser;
import com.devcamp.task6070jparelationshipordercustomer.repository.IUserRepository;
import com.devcamp.task6070jparelationshipordercustomer.service.UserService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;
   
    //get all user list
    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUsers(){
        try {
            return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    //get user detail by id
    @Autowired
    IUserRepository userRepository;
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable("id") long id){
        try {
            CUser user = userRepository.findById(id);
            if (user != null){
                return new ResponseEntity<>(user, HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }

    //create new user
    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody CUser pUser){
        try {
            pUser.setCreated(new Date());
            pUser.setUpdated(null);
            CUser _user = userRepository.save(pUser);
            return new ResponseEntity<>(_user, HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to create specified user: "+e.getCause().getCause().getMessage());

        }
    }

    //update user
    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name="id")long id, @Valid @RequestBody CUser pUserUpdate){
        try {
           CUser user = userRepository.findById(id);
            if (user != null){
                user.setFullname(pUserUpdate.getFullname());
                user.setEmail(pUserUpdate.getEmail());
                user.setPhone(pUserUpdate.getPhone());
                user.setAddress(pUserUpdate.getAddress());
                user.setUpdated(new Date());
                
                return new ResponseEntity<>(userRepository.save(user), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to update specified user: "+e.getCause().getCause().getMessage());

        }
    }
    //delete user by Id
    @DeleteMapping("/users/{id}")
	public ResponseEntity<CUser> deleteUserById(@PathVariable("id") long id) {
		try {
			userRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    //task 5 count user
    @GetMapping("/users-count")
	public long countUser() {
		return userRepository.count();
	}
	
    //task 6 check user by id
	@GetMapping("/users/check/{id}")
	public boolean checkUserById(@PathVariable Long id) {
		return userRepository.existsById(id);
	}
    //task 7-pageable
    @GetMapping("/users5")
    public ResponseEntity<List<CUser>> getFiveUsers(@RequestParam(value = "page", defaultValue = "1") String page,
	@RequestParam(value = "size", defaultValue = "5") String size){
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CUser> userList = new ArrayList<CUser>();
            userRepository.findAll(pageWithFiveElements).forEach(userList::add);
            return new ResponseEntity<>(userList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

}
